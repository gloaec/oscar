const { MessageEmbed } = require("discord.js");
const fs = require('fs');
const util = require('util');
const path = require('path');
const exec = util.promisify(require('child_process').exec);
const {
  REPOSITORIES = [],
  REPOSITORIES_PATH = "./repositories",
  EMOJIS = [],
  TIMEOUT = 60,
  ANONYMOUS = false,
  REVOTE = false,
  AUTHOR_BLACKLIST = [],
  COMMIT_BLACKLIST = [],
  AFTER = null,
  CHECK_ICON = "✅",
} = require("../util/EvobotUtil");

module.exports = {
  name: "play",
  cooldown: 5,
  aliases: ["git"],
  description: "Picks a commit message and asks for author.",
  async execute(message) {
    const permissions = message.channel.permissionsFor(message.client.user);
    if (!permissions.has(["MANAGE_MESSAGES", "ADD_REACTIONS"]))
      return message.reply("Missing permission to manage messages or add reactions");

    // Parse commits
    const COMMIT_DELIMITER = "XXXCOMMITXXX"
    const FIELD_DELIMITER = "|||"
    const commit_format = ["%an", "%ar", "%B"].join(FIELD_DELIMITER)
    const options = [];
    const commits = [];
    const leaderboard = [];
    const iconUrl = message.guild.iconURL();
    const questionCount = 0;

    if(AFTER) {
      options.push(`--after={${AFTER}}`);
    }

    //for(repository of REPOSITORIES) {
    await Promise.all(REPOSITORIES.map(async repository => {
      const repoName = path.parse(repository.split(/[\/]+/).pop()).name;
      console.log(`Reading ${repoName}...`);
      const initMessage = await message.channel.send(`\`${repoName}\`: Analyzing...`);
      const repoPath = [REPOSITORIES_PATH, repoName].join('/')
      if (!fs.existsSync(REPOSITORIES_PATH)) {
        await exec(`mkdir -p ${REPOSITORIES_PATH}`);
      }
      if (!fs.existsSync(repoPath)) {
        console.log(`\`${repoName}\`: Cloning...`);
        await initMessage.edit(`\`${repoName}\`: Cloning...`);
        await exec(`git clone ${repository}`, { cwd: REPOSITORIES_PATH }).catch(e => console.error(e));
      }
      const command = `git fetch ${repository} && git log --no-merges --pretty="${COMMIT_DELIMITER}${commit_format}" ${options.join(' ')}`;
      const raw_commits = await exec(command, { cwd: repoPath }).then(({ err, stdout, stderr }) => {
        if (err) {
          console.error(err);
          return [];
        }
        return stdout.split(COMMIT_DELIMITER);
      }).catch(e => {
        console.error(e);
        return [];
      });
      console.log(`${raw_commits.length} commits`);
      await initMessage.edit(`\`${repoName}\`: Found ${raw_commits.length} commits`);
      for (c of raw_commits) {
        if(!c.trim().length) {
          continue;
        }
        const fields = c.split(FIELD_DELIMITER)
        commits.push({ author: fields[0], date: fields[1], msg: fields[2] });
      }
      await initMessage.delete();
    }));

    const committers = [...new Set(commits.map(c => c.author).filter(Boolean))];

    console.log(`You're playing on ${REPOSITORIES.length} repos with ${commits.length} commits and ${committers.length} committers`);

    const startEmbed = await message.channel.send(getStartEmbed(commits.length, committers, leaderboard, iconUrl, questionCount))

    // Game loop

    const shuffledCommits = shuffle(commits);

    if(!commits.length) {
      console.error("No commits found");
      return;
    }

    await newQuestion(message, commits.length, shuffledCommits, committers, startEmbed, leaderboard, iconUrl, questionCount);

  }
};

async function newQuestion(message, commits_length, shuffledCommits, committers, startEmbed, leaderboard, iconUrl, questionCount) {

  const commit = shuffledCommits.pop();
  const { author, msg, date } = commit;

  const skip = !author || !msg
    || COMMIT_BLACKLIST.find(c => msg.includes(c))
    || AUTHOR_BLACKLIST.find(a => author == a)
  ;

  if(skip) {
    return await newQuestion(message, commits_length, shuffledCommits, committers, startEmbed, leaderboard, iconUrl, questionCount);
  }

  let timer = TIMEOUT;
  let choices = shuffle(committers).slice(0, 4);

  if(!choices.includes(author)) {
    choices.pop();
    choices.push(author);
  }

  choices = shuffle(choices);

  const questionEmbed = await message.channel.send(
    getQuestionEmbed(commit, choices, false, timer)
  );

  try {
    await Promise.all(choices.map((c, i) =>
      questionEmbed.react(EMOJIS[i])
    ))
    await questionEmbed.react(CHECK_ICON);
  } catch (error) {
    console.error(error);
    message.channel.send(error.message).catch(console.error);
  }

  const answers = [];
  const answersFilter = (reaction, user) => EMOJIS.includes(reaction.emoji.name);
  const commandFilter = (reaction, user) => reaction.emoji.name == CHECK_ICON;
  const answersCollector = questionEmbed.createReactionCollector(answersFilter, { time: TIMEOUT*1000 });
  const commandCollector = questionEmbed.createReactionCollector(commandFilter, { time: TIMEOUT*1000 });

  answersCollector.on("collect", async (reaction, user) => {
    try {
      await reaction.users.remove(user.id);
      const answer = answers.find(({ user: u }) => user == u);
      const choice = choices[EMOJIS.indexOf(reaction.emoji.name)];
      if(answer){
        if(REVOTE){
          answer.message.edit(`${user} voted ${ANONYMOUS ? '' : choice}`);
          answer.choice = choice;
        } else {
          answer.message.edit(`${user} voted ${ANONYMOUS ? '' : answer.choice} (and attempted to cheat...${answer.count == 2 ? ' twice...': answer.count > 2 ? ` ${answer.count} times !` : ''})`);
          answer.count++;
        }
      } else {
        const m = await message.channel.send(`${user} voted ${ANONYMOUS ? '' : choice}`);
        answers.push({ user, choice, message: m, count: 1 });
      }
      //questionEmbed.edit(getQuestionEmbed(commit, choices, false, timer));
    } catch (error) {
      console.error(error);
      return message.channel.send(error.message).catch(console.error);
    }
  });

  commandCollector.on("collect", async (reaction, user) => {
    await reaction.users.remove(user.id);
    if(user.id == message.author.id) {
      if(reaction.emoji.name == CHECK_ICON) {
        terminate();
      }
    }
  });

  const interval = setInterval(async () => {
    if(timer >= 0) {
      if(timer % 5 == 0){
        questionEmbed.edit(getQuestionEmbed(commit, choices, false, timer));
      }
      timer--;
    } else {
      terminate();
    }
  }, 1000);

  const terminate = async () => {
    clearInterval(interval);
    timer = -1;
    const winners = answers.filter(({ choice }) => choice == author).map(({ user }) => user);
    questionEmbed.reactions.removeAll();
    questionEmbed.edit(getQuestionEmbed(commit, choices, true, timer, winners));
    answers.map(async answer => {
      let entry = leaderboard.find(({ user }) => user == answer.user);
      if(!entry) {
        const index = leaderboard.push({ user: answer.user, score: 0 }) - 1;
        entry = leaderboard[index];
      }
      if(answer.choice == author){
        entry.score++;
      }
    });
    await Promise.all(answers.map(({ message }) => message.delete()));

    leaderboard = leaderboard.sort(({ score: a }, { score: b }) => b - a);

    questionCount++;
    startEmbed.edit(getStartEmbed(commits_length, committers, leaderboard, iconUrl, questionCount));

    try {
      await questionEmbed.react("⏹");
      await questionEmbed.react("▶️");
    } catch (error) {
      console.error(error);
      message.channel.send(error.message).catch(console.error);
    }

    const filter2 = (reaction, user) =>
      ["⏹","▶️"].includes(reaction.emoji.name) && message.author.id === user.id;
    const collector2 = questionEmbed.createReactionCollector(filter2);
    collector2.on("collect", async (reaction) => {
      if(reaction.emoji.name == "⏹") {
        questionEmbed.delete();
      } else if (reaction.emoji.name == "▶️") {
        questionEmbed.delete();
        newQuestion(message, commits_length, shuffledCommits, committers, startEmbed, leaderboard, iconUrl, questionCount);
      }
    });
  }
}

function getStartEmbed(commits_length, committers = [], leaderboard = [], iconUrl = null, questionCount) {
  let gameDescription =
    "The goal of the git game is to guess committers based on their commit messages."
  ;
  if(AFTER) {
    gameDescription += `\n- Since ${AFTER}`;
  }
  const embed = new MessageEmbed()
    .setTitle("The Git Game\n")
    .setThumbnail(iconUrl)
    .setColor("#F05133")
    .setDescription(gameDescription)
    .addFields({
      name: 'Repositories',
      value: REPOSITORIES.length,//join("\n"),
      inline: true
    },{
      name: 'Authors',
      value: committers.length,//join("\n"),
      inline: true
    },{
      name: 'Commits',
      value: commits_length,
      inline: true
    })
  ;
  if(leaderboard.length) {
    embed.addFields({
      name: `Leaderboard (${questionCount} questions)`,
      value:leaderboard.map(({ user, score }) => `${user}: ${score}`).join("\n")
    });
  }
  return embed;
}

function getQuestionEmbed(commit, choices, answer = false, timer, winners = []) {
  const { author, msg, date } = commit;
  let description = [
    date,
    `\`${msg.trim()}\`\n`,
    answer
      ? `:white_check_mark: **${author}**`
      : choices.map((c, i) => answer
        ? `${author == c ? ':white_check_mark:' : ':x:'}`
        : `${EMOJIS[i]} ${c}`
    ).join("\n")
  ].join("\n");

  if(winners.length) {
    description += `\n\nWinners: ${winners.join(', ')}`;
  } else if(timer < 0) {
    description += `\n\nNo winners`;
  }

  return new MessageEmbed()
    .setTitle(answer ? '' : "Who wrote it ?")
    .setColor(answer ? '' : "#F05133")
    .setDescription(description)
    .setFooter(timer >= 0 ? `${timer} seconds remaining` : '')
  ;
}

function shuffle(a) {
  return a.sort(() => 0.5 - Math.random());
}
