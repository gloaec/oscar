const { MessageEmbed } = require("discord.js");
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const { Gitlab } = require( '@gitbeaker/node');
const {
  //EMOJIS = [],
  TIMEOUT = 60,
  ANONYMOUS = false,
  REVOTE = false,
  CHECK_ICON = "✅",
  GITLAB_TOKEN
} = require("../util/EvobotUtil");

const WEIGHTS = [1,2,3,5,8,13,20,40,100];

const api = new Gitlab({
  token: GITLAB_TOKEN,
});

module.exports = {
  name: "poker",
  cooldown: 5,
  aliases: ["weight"],
  description: "Collaborative weighting of issue",
  async execute(message, args) {
    const permissions = message.channel.permissionsFor(message.client.user);
    if (!permissions.has(["MANAGE_MESSAGES", "ADD_REACTIONS"]))
      return message.reply("Missing permission to manage messages or add reactions");

    //message.delete();

    let timer = TIMEOUT;

    const { issue, groupName, subGroupName, projectName } = await getIssue(args[0]);

    if(!issue) {
      return message.reply("Issue not found");
    }

    const emojis = await Promise.all(WEIGHTS.map(w =>
      message.guild.emojis.cache.find(emoji => emoji.name === `w${w}`) ||
      message.guild.emojis.create(`./assets/${w}.png`, `w${w}`)
    ));
    const emojiIds = emojis.map(({ id }) => id);

    const issueEmbed = await message.channel.send(getIssueEmbed(issue, timer));

    await Promise.all(emojis.map((e, i) =>
      issueEmbed.react(e)
    ))
    await issueEmbed.react(CHECK_ICON);

    const answers = [];
    const answersFilter = (reaction, user) => emojiIds.includes(reaction.emoji.id);
    const commandFilter = (reaction, user) => reaction.emoji.name == CHECK_ICON;
    const answersCollector = issueEmbed.createReactionCollector(answersFilter, { time: TIMEOUT*1000 });
    const commandCollector = issueEmbed.createReactionCollector(commandFilter, { time: TIMEOUT*1000 });

    answersCollector.on("collect", async (reaction, user) => {
      try {
        await reaction.users.remove(user.id);
        const answer = answers.find(({ user: u }) => user == u);
        const weight = WEIGHTS[emojiIds.indexOf(reaction.emoji.id)];
        if(answer){
          if(REVOTE){
            answer.message.edit(`${user} voted ${ANONYMOUS ? '' : weight}`);
            answer.weight = weight;
          } else {
            answer.message.edit(`${user} voted ${ANONYMOUS ? '' : answer.weight} (and attempted to cheat...${answer.count == 2 ? ' twice...': answer.count > 2 ? ` ${answer.count} times !` : ''})`);
            answer.count++;
          }
        } else {
          const m = await message.channel.send(`${user} voted ${ANONYMOUS ? '' : weight}`);
          answers.push({ user, weight, message: m, count: 1 });
        }
      } catch (error) {
        console.error(error);
        return message.channel.send(error.message).catch(console.error);
      }
    });

    commandCollector.on("collect", async (reaction, user) => {
      await reaction.users.remove(user.id);
      if(user.id == message.author.id) {
        if(reaction.emoji.name == CHECK_ICON) {
          terminate();
        }
      }
    });

    const interval = setInterval(async () => {
      if(timer >= 0) {
        if(timer % 5 == 0){
          issueEmbed.edit(getIssueEmbed(issue, timer));
        }
        timer--;
      } else {
        terminate();
      }
    }, 1000);

    const terminate = async function() {
      clearInterval(interval);
      timer = 0;
      await issueEmbed.delete();
      await Promise.all(answers.map(({ message }) => message.delete()));
      const sum = answers.map(({ weight }) => weight).reduce((a, b) => a + b, 0);
      const avg = Math.round(sum / answers.length);
      const issueWeightEmbed = await message.channel.send(getIssueWeightEmbed(issue, answers, avg));
      if(answers.length) {
        await api.Issues.edit([groupName, subGroupName, projectName].filter(Boolean).join('/'), issue.iid, { weight: avg });
      }
    }
  }
};

function getIssueEmbed(issue, timer) {
  return new MessageEmbed()
    .setTitle(issue.title)
    .setURL(issue.web_url)
    .setColor("#FCA386")
    .setDescription(truncateString(issue.description, 5000))
    .setFooter(timer >= 0 ? `${timer} seconds remaining` : '')
}

function getIssueWeightEmbed(issue, answers, avg) {
  return new MessageEmbed()
    .setTitle(issue.title)
    .setURL(issue.web_url)
    .setColor("#FC6D27")
    .addFields({
      name: 'Users',
      value: answers.map(({ user }) => user).join("\n"),
      inline: true
    },{
      name: 'Weight',
      value: answers.map(({ weight }) => weight).join("\n"),
      inline: true
    }, {
      name: 'Average weight',
      value: avg,
      inline: true
    })
}

async function getIssue(descriptor) {
  try {
    if(/^https?:\/\//.test(descriptor)) {
      let _, subGroupName, groupName, projectName, issueIid;
      let result = descriptor.match(/^https?:\/\/([^\/]+)\/([^\/]+)\/([^\/]+)\/-\/issues\/(\d+)/)
      if (result) {
        [_, repoDomain, groupName, projectName, issueIid] = result;
      }
      if (!issueIid) {
        result = descriptor.match(/^https?:\/\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/-\/issues\/(\d+)/)
        if(result) {
          [_, repoDomain, groupName, subGroupName, projectName, issueIid] = result;
        }
      }
      //const [group] = await api.Groups.all({ search: groupName });
      //const [project] = await api.Projects.all({ search: [projectName].filter(Boolean).join('/') });
      console.log("api.Issues.show(", [groupName, subGroupName, projectName].filter(Boolean).join('/'), ',', issueIid, ')');
      const issue = await api.Issues.show([groupName, subGroupName, projectName].filter(Boolean).join('/'), issueIid);
      return { issue, repoDomain, groupName, subGroupName, projectName };
    } else if(/^[^#]+#\d+$/.test(descriptor)) {
      const [projectDescriptor, issueIid] = descriptor.split('#');
      return {};
    }
  } catch(e) {
    console.error(e);
  }
  return {};
}

async function weightIssue(issueDescriptor) {
  
}

function fib(index) {
  return [1,2,3,5,8,13,20,40,100][index]
}

function truncateString(str, num) {
  if (str.length > num) {
    return str.slice(0, num) + "...";
  } else {
    return str;
  }
}

