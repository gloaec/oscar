const { MessageEmbed } = require("discord.js");
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const {
  EMOJIS = [],
  TIMEOUT = 60,
  ANONYMOUS = false,
  REVOTE = false,
} = require("../util/EvobotUtil");

module.exports = {
  name: "poll",
  cooldown: 5,
  aliases: ["vote"],
  description: "Starts a new poll",
  async execute(message, args) {
    const permissions = message.channel.permissionsFor(message.client.user);
    if (!permissions.has(["MANAGE_MESSAGES", "ADD_REACTIONS"]))
      return message.reply("Missing permission to manage messages or add reactions");

    console.log(message.content, args);
    // Parse args

  }
};
