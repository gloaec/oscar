FROM node:12.19-slim

ENV USER=oscar

# install python, git and make
RUN apt-get update && \
	apt-get install -y git python3 build-essential && \
	apt-get purge -y --auto-remove
	
# create evobot user
RUN groupadd -r ${USER} && \
	useradd --create-home --home /home/oscar -r -g ${USER} ${USER}
	
# set up volume and user
USER ${USER}
WORKDIR /home/oscar

RUN mkdir repositories

COPY package*.json ./
RUN npm install
VOLUME [ "/home/oscar" ]

COPY . .

ENTRYPOINT [ "node", "index.js" ]
