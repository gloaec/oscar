let config;

try {
  config = require("../config.json");
} catch (error) {
  console.error(error);
}

exports.TOKEN = config ? config.TOKEN : process.env.TOKEN;
exports.GITLAB_TOKEN = config ? config.GITLAB_TOKEN : process.env.GITLAB_TOKEN;
exports.PREFIX = config ? config.PREFIX : process.env.PREFIX;
exports.REPOSITORIES = config ? config.REPOSITORIES : process.env.REPOSITORIES;
exports.REPOSITORIES_PATH = config ? config.REPOSITORIES_PATH : process.env.REPOSITORIES_PATH;
exports.EMOJIS = config ? config.EMOJIS : process.env.EMOJIS;
exports.TIMEOUT = config ? config.TIMEOUT : process.env.TIMEOUT;
exports.ANONYMOUS = config ? config.ANONYMOUS : process.env.ANONYMOUS;
exports.REVOTE = config ? config.TIMEOUT : process.env.REVOTE;
exports.AUTHOR_BLACKLIST = config ? config.AUTHOR_BLACKLIST : process.env.AUTHOR_BLACKLIST;
exports.COMMIT_BLACKLIST = config ? config.COMMIT_BLACKLIST : process.env.COMMIT_BLACKLIST;
exports.AFTER = config ? config.AFTER : process.env.AFTER;
